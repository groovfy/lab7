package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddNewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		
		Intent d = this.getIntent();
		if(d.hasExtra("id")){
			String name = d.getStringExtra("name");
			String phone = d.getStringExtra("phone");
			int ctype = d.getIntExtra("type", 0);
			String email = d.getStringExtra("email");
			
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			//RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			
			etName.setText(name);
			etPhone.setText(phone);
			etEmail.setText(email);
			if (ctype == R.drawable.home) {
				RadioButton rdHome = (RadioButton)findViewById(R.id.rdHome);
				rdHome.setChecked(true);
			}
			else{
				RadioButton rdMobile = (RadioButton)findViewById(R.id.rdMobile);
				rdMobile.setChecked(true);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			
			String sName = etName.getText().toString().trim();
			String sPhone = etPhone.getText().toString().trim();
			String sEmail = etEmail.getText().toString().trim();
			int iType = rdg.getCheckedRadioButtonId();
			
			if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
				Toast t = Toast.makeText(this, 
						"Contact name,  phone, and type are required", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent d = this.getIntent();
				Intent data = new Intent();
				
				data.putExtra("name", sName);
				data.putExtra("phone", sPhone);
				data.putExtra("email", sEmail);
				data.putExtra("id", d.getLongExtra("id", 0));
				switch(iType) {
				case R.id.rdHome:
					data.putExtra("type", String.valueOf(R.drawable.home));
					break;
				case R.id.rdMobile:
					data.putExtra("type", String.valueOf(R.drawable.mobile));
					break;
				}
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
